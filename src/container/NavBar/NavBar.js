import React from 'react';
import Logo from '../../component/Logo/Logo'
import NavItems from "../NavItems/NavItems";
 const NavBar = (props)=>{
    let LogoLabel = "ram";
    return(<nav>
       <Logo color={'red'} label={LogoLabel}/>
      <NavItems items={props.navItems}/>
    </nav>)
}


export default NavBar;