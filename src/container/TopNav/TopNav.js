import React from 'react';
import Logo from "../../component/Logo/Logo";
import NavItems from "../NavItems/NavItems";


const TopNav = props =>(

    <div className="top-nav">
        <Logo/>
        <NavItems/>
    </div>

);

export default TopNav