import React,{Component} from 'react';
import NavBar from "../../container/NavBar/NavBar";
import Home from "../../view/Home/Home";




class Layout extends Component{
    state={
        navItems:[
            {id:1 , label:'Home'},
            {id:1 , label:'Services'},
            {id:1 , label:'About'},
            {id:1 , label:'Products'},
            {id:1 , label:'Contacts'},

        ]
    }
    render() {
        return(
            <main>
                <NavBar navItems={this.state.navItems}/>
                <Home/>
            </main>
        );
    }
}

export default Layout;