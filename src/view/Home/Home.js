import React, { Component } from 'react';

class Home extends Component{
    render(){
        return(<section>
            This is home Component {this.props.phrase}
        </section>)
    }
}
export default Home;